from django.contrib import admin
from .models import *
# Register your models here.

@admin.register(Blog)
class BlogAdmin(admin.ModelAdmin):
    list_display = ("title", "author", "category", "createdate", "status", "is_enable")

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("codename", "name", "createdate", "is_enable")

@admin.register(Word)
class WordAdmin(admin.ModelAdmin):
    list_display = ("word", "type", "pinyin", "meaning", "sentence")

@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
    list_display = ("blog", "content")