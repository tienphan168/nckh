function render_translate(data,type){
    if(type=='word'){
        let tran =`
        <div class="trans-content"><div>${data.word} - ${data.pinyin}</div>
        <div>${data.type} ${data.meaning}</div></div>`
        document.getElementById('tran-area').innerHTML=tran
        document.getElementById('tran-area').style.width='fit-content'
    }
    else{
        // document.getElementById('tran-area').style.width='72%'
        let tran =`<div class="trans-content">${data.translation}</div>`
        document.getElementById('tran-area').innerHTML=tran
    }

}

var ids = [
    'btv',
    'content',
    'exercise',
  ]
  
  function show(id) {
    ids.forEach((id) => {
      var div = document.querySelector('#' + id)
      div.style.display = 'none'
    })
    var div = document.querySelector('#' + id)
    div.style.display = 'flex'
  }