from django.urls import path
from django.conf.urls import url
from django.conf import settings
from django.views.static import serve

from . import views, ajax

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('blogs/', views.blogs, name='blogs'),
    path('blogs/<int:id>', views.blogdetail, name='blogdetail'),
    path('about/', views.about, name='about'),
    path('blogs/<int:id>/ajax/', ajax.translation, name='translation'),
    path('blogs/<int:id>/ajax/word', ajax.trans_word, name='trans_word'),
    path('blogs/<int:id>/ajax/sentence', ajax.trans_sentence, name='trans_sentence'),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': 
		settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve, {'document_root': 
        settings.STATIC_ROOT}),
]