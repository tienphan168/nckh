from posixpath import lexists, split
from django.shortcuts import render, get_object_or_404
from .models import *
from django.http import JsonResponse
import json, re
import numpy as np

def swapPositions(list, pos1, pos2):
     
    list[pos1], list[pos2] = list[pos2], list[pos1]
    return list

# def translation(request, id):
#     blog = get_object_or_404(Blog, id=id, is_enable=True)
#     temp = ""
#     result = []
#     for i in blog.content:
#         temp += i
#         if i == '。':
#             result.append(temp)
#             temp = ""

#     lst = [i.split('/') for i in result]
#     print(lst)



#     data = {
#         'list': lst,
#     }
#     return JsonResponse(data)

def translation(request, id):
    blog = get_object_or_404(Blog, id=id, is_enable=True)
    temp = ""

    # split_paragraph = [i.split('|') for i in blog.content]
    
    # print(split_paragraph)
    paragraph = []
    for i in blog.content:
        temp += i
        if i == '|':
            paragraph.append(temp)
            temp = ""

    print(paragraph, '\n')

    split_paragraph = [i.split('|') for i in paragraph]
    print(split_paragraph, '\n')

    split_sentence = [item[0].split('.') for item in split_paragraph]
    print(split_sentence, '\n')

    newlist=[]
    for i in split_sentence:
        tmp=[]
        for j in i:
            tmp.append(j.split('/'))
        newlist.append(tmp)
            # print(split_sentence[i])
    print(newlist)

    data = {
        'list': newlist,
    }

    return JsonResponse(data)

def trans_word(request, id):
    word = request.GET.get('word')
    if word:
        flag = True
    else:
        flag = False
    blog = Blog.objects.get(id=id)
    dictionary = Word.objects.get(blog=blog, word=word)
    print(dictionary)
    data = {
        'word': dictionary.word,
        'type': dictionary.type,
        'pinyin': dictionary.pinyin,
        'meaning': dictionary.meaning,
        'sentence': dictionary.sentence,
        'flag': flag,
    }

    return JsonResponse(data)

def trans_sentence(request, id):
    ids = request.GET.get('id')
    blog = get_object_or_404(Blog, id=id, is_enable=True)

    arr = re.split(r"[.]\s*", blog.translation)
    # test = np.array(arr)
    # swapPositions(arr, 1, 0)
    
    # print(int(id))
    count = 0
    for i in arr:
        # print(i, '\n')
        for j in range(len(i)):
            if i[j] == '/':
                # print('ok')
                arr[count] = arr[count].replace("/", "")
                swapPositions(arr, int(ids) + 1, int(ids))
        count += 1
        #     print(int(arr[i][j]))
            # temp = arr[i]
            # if temp[j] == '/':
            #     swapPositions(arr, int(id+1), int(id))
    test = np.array(arr)
    # print(id)

    data = {
       'translation': test[int(ids)]
    }
    return JsonResponse(data)