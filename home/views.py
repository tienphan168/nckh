from django.shortcuts import render, get_object_or_404
from .models import *
import json
from django.http import JsonResponse



def index(request):
     blogs = Blog.objects.all().order_by('-createdate').exclude(is_enable=False)

     context = {
          'blogs': blogs,
     }
     return render(request, 'homepage/index.html', context)

def blogs(request):
     blogs = Blog.objects.all().order_by('-createdate').exclude(is_enable=False)

     context = {
          'blogs': blogs,
     }
     return render(request, 'homepage/blog.html', context)

def blogdetail(request, id):
     blog = get_object_or_404(Blog, id=id, is_enable=True)

     context = {
          'blog': blog,
     }
     return render(request, 'homepage/blog-detail.html', context)

def about(request):
     return render(request, 'homepage/about.html')