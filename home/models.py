from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
from urllib.parse import quote
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.
class Category(models.Model):
	id = models.AutoField(primary_key=True)
	codename = models.CharField(max_length=250, blank=True, null=True, unique=True)
	name = models.CharField(max_length=250, blank=True, null=True)
	note = models.CharField(max_length=250, blank=True, null=True)
	createdate = models.DateField(auto_now_add=True, blank=True, null=True)
	editdate = models.DateTimeField(auto_now=True, blank=True, null=True)
	is_enable = models.BooleanField(default=True, blank=True, null=True)

	def __str__(self):
		return self.codename

	class Meta:
		managed = True
		db_table = 'Category'

class Blog(models.Model):
	STATUS_CHOICES = (
		('d', 'Draft'),
		('p', 'Published'),
	) 
	author = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=True, null=True)
	id = models.AutoField(primary_key=True)
	category = models.ForeignKey('Category', models.DO_NOTHING, null=True)
	title = models.CharField(max_length=100)
	photo = models.CharField(max_length=250, blank=True, null=True)
	# file = models.FileField(upload_to='musics/')
	audio = models.CharField(max_length=250, blank=True, null=True)
	content = models.TextField(blank=True, null=True)
	translation = models.TextField(blank=True, null=True)
	text = models.TextField(blank=True, null=True)
	status = models.CharField(max_length=1, choices=STATUS_CHOICES)
	# slug = models.SlugField(max_length=250, null=True, blank=True)
	createdate = models.DateField(auto_now_add=True, blank=True, null=True)
	editdate = models.DateTimeField(auto_now=True, blank=True, null=True) 
	is_enable = models.BooleanField(default=True, blank=True, null=True)
	note = models.TextField(blank=True, null=True)

	def __str__(self):
		return self.title

	def save(self, *args, **kwargs):
		self.slug = slugify(self.title)
		super(Blog, self).save(*args, **kwargs)

	class Meta:
		ordering = ['-createdate']
		managed = True
		db_table = 'Blog'

class Word(models.Model):
	word = models.CharField(max_length=100, blank=True, null=True)
	blog = models.ForeignKey('Blog', models.CASCADE, null=True)
	type = models.CharField(max_length=100, blank=True, null=True)
	pinyin = models.CharField(max_length=100, blank=True, null=True)
	meaning = models.CharField(max_length=100, blank=True, null=True)
	sentence = models.TextField(blank=True, null=True)

	class Meta:
		managed = True
		db_table = 'Word'


class Table(models.Model):
	id = models.AutoField(primary_key=True)
	blog = models.ForeignKey('Blog', models.CASCADE, null=True)
	content = RichTextUploadingField(blank=True, null=True)

	class Meta:
		managed = True
		db_table = 'Table'
